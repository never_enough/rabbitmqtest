package com.java1234.consumer;

import com.java1234.consumer.service.RabbitMqService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;


@SpringBootApplication
public class ConsumerApplication {

    public static void main(String[] args) {
        ApplicationContext ac=SpringApplication.run(ConsumerApplication.class,args);
       /*
        RabbitMqService rabbitMqService= (RabbitMqService) ac.getBean("rabbitmqService");
        rabbitMqService.receiveMessage();  // 手动抓取一条消息
        */
    }
}
