package com.java1234.consumer.service.impl;

import com.java1234.consumer.service.RabbitMqService;
import com.java1234.producer.config.RabbitMQConfig;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import com.rabbitmq.client.Channel;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.messaging.handler.annotation.Header;

import java.util.Date;

@Service("rabbitmqService")
public class RabbitMqServiceImpl implements RabbitMqService {

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    CachingConnectionFactory cachingConnectionFactory;

    @Bean(name="limitContainerFactory")
    public SimpleRabbitListenerContainerFactory simpleRabbitListenerContainerFactory(){
        SimpleRabbitListenerContainerFactory factory=new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(cachingConnectionFactory);
        factory.setPrefetchCount(3);  // 每次最多拿3个，等这三个处理完之后，再去队列中拿第二批，起到了限流的作用
        return factory;
    }

    @Override
    @RabbitListener(queues = {RabbitMQConfig.TTL_DIRECT_QUEUE})
    public void receiveTTLMessage(String message,Channel channel,@Header(AmqpHeaders.DELIVERY_TAG) long deliveryTag) {
        try {
            System.out.println(System.currentTimeMillis()+"接收到的mq消息："+message);
            // 处理业务异常  --> 拒签
            System.out.println("处理业务"+1/0);
            // long deliveryTag, boolean multiple
            System.out.println("deliveryTag="+deliveryTag);
            channel.basicAck(deliveryTag,true);
          /*  if(deliveryTag==5){
                channel.basicAck(deliveryTag,true);
            }*/
        }catch(Exception e){
            e.printStackTrace();
            // long deliveryTag, boolean multiple, boolean requeue
            try {
                channel.basicNack(deliveryTag,false,false);
                // long deliveryTag, boolean requeue
                // channel.basicReject(deliveryTag,true);
                // Thread.sleep(1000);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }


    /*
     *   通过channel的basicAck方法手动签收，通过basicNack方法拒绝签收
     * 注： concurrency = "5-8"  与 containerFactory = "limitContainerFactory" 当然可以同时使用，但推荐还是使用一个
     */
    @Override
    @RabbitListener(queues = {RabbitMQConfig.DIRECT_QUEUE}, containerFactory = "limitContainerFactory")
    public void receiveMessage(String message,Channel channel,@Header(AmqpHeaders.DELIVERY_TAG) long deliveryTag) {
        try{
            //String message= (String)amqpTemplate.receiveAndConvert(RabbitMQConfig.DIRECT_QUEUE);
            System.out.println(System.currentTimeMillis() + "接收的mq消息："+message);

            // 业务处理 异常测试
            // System.out.println("业务处理"+1/0);

            // long deliveryTag 消息接收tag boolean multiple 是否批量确认
            System.out.println("deliveryTag="+deliveryTag);

            /**
             * 无异常就确认消息
             * basicAck(long deliveryTag, boolean multiple)
             * deliveryTag:取出来当前消息在队列中的的索引;
             * multiple:为true的话就是批量确认,如果当前deliveryTag为5,那么就会确认
             * deliveryTag为5及其以下的消息;一般设置为false
             */

//            if(deliveryTag==5){
//                channel.basicAck(deliveryTag,true);
//            }
            channel.basicAck(deliveryTag,true);

        }catch (Exception e){

            e.printStackTrace();

            /**
             * 有异常就绝收消息
             * basicNack(long deliveryTag, boolean multiple, boolean requeue)
             * requeue:true为将消息重返当前消息队列,还可以重新发送给消费者;
             *         false:将消息丢弃
             */

            // long deliveryTag, boolean multiple, boolean requeue

            try {

                channel.basicNack(deliveryTag,false,true);
                // long deliveryTag, boolean requeue
                // channel.basicReject(deliveryTag,true);

                //Thread.sleep(1000);     // 这里只是便于出现死循环时查看

                /*
				 * 一般实际异常情况下的处理过程：记录出现异常的业务数据，将它单独插入到一个单独的模块，
				 * 然后尝试3次，如果还是处理失败的话，就进行人工介入处理
				*/

            } catch (Exception e1) {
                e1.printStackTrace();
            }

        }

    }


    @Override
    @RabbitListener(queues = {RabbitMQConfig.TOPIC_QUEUE1})
    public void receiveMessage1(String message) {
        //System.out.println("消费者1接收到的mq消息："+message);

        System.out.println("队列1接收日志消息："+message);
    }

/*    @Override
    @RabbitListener(queues = {RabbitMQConfig.TOPIC_QUEUE2})
    public void receiveMessage2(String message) {
        //System.out.println("消费者2接收到的mq消息："+message);

        System.out.println("队列2接收日志消息："+message);
    }*/
    @Override
    @RabbitListener(queues = {RabbitMQConfig.DELAYED_DIRECT_QUEUE})
    public void receiveMessage2(String message) {
        System.out.println("接收延迟消息："+message+":"+new Date().toString());
    }

    @Override
    @RabbitListener(queues = {RabbitMQConfig.SUB_QUEUE1})
    public void receiveSubMessage1(String message) {
        System.out.println("订阅者1：接收到的mq消息："+message);
    }

    @Override
    @RabbitListener(queues = {RabbitMQConfig.SUB_QUEUE2})
    public void receiveSubMessage2(String message) {
        System.out.println("订阅者2：接收到的mq消息："+message);
    }
}
