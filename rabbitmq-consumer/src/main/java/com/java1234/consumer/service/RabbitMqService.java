package com.java1234.consumer.service;

import com.rabbitmq.client.Channel;

public interface RabbitMqService {

    /**
     * 接收消息
     */
    public void receiveTTLMessage(String message,Channel channel,long deliveryTag);

    /**
     * 接收消息
     */
    public void receiveMessage(String message,Channel channel,long deliveryTag);

    /**
     * 接收消息
     */
    public void receiveMessage1(String message);

    /**
     * 接收消息
     */
    public void receiveMessage2(String message);

    /**
     * 接收订阅消息1
     */
    public void receiveSubMessage1(String message);

    /**
     * 接收订阅消息2
     */
    public void receiveSubMessage2(String message);
}
