package com.java1234.producer;

import com.java1234.producer.service.RabbitMqService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import static java.lang.Thread.sleep;


@SpringBootApplication
public class ProducerApplication {

    public static void main(String[] args) throws InterruptedException {
        ApplicationContext ac=SpringApplication.run(ProducerApplication.class,args);
        RabbitMqService rabbitMqService= (RabbitMqService) ac.getBean("rabbitmqService");

        rabbitMqService.sendDelayedMessage("测试延迟消息20秒",20000);
        rabbitMqService.sendDelayedMessage("测试延迟消息35秒",35000);

    }
}
