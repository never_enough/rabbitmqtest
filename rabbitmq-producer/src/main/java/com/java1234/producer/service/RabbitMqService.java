package com.java1234.producer.service;


public interface RabbitMqService {

    /**
     * 发送消息
     * @param message
     */
    public void sendMessage(String message);
    /**
     * 发送消息
     * @param message
     */
    public void sendFanoutMessage(String message);


    /**
     * 发送路由模式消息
     */
    public void sendRoutingMessage();

    /**
     * 发送Topic模式消息
     */
    public void sendTopicMessage();

    /**
     * 发送TTL消息
     * @param message
     */
    public void sendTTLMessage(String message);

    /**
     * 发送延迟消息
     * @param message
     * @param delayTime
     */
    public void sendDelayedMessage(String message,Integer delayTime);
}
