package com.java1234.producer.service.impl;

import com.java1234.producer.config.RabbitMQConfig;
import com.java1234.producer.service.RabbitMqService;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;


@Service("rabbitmqService")
public class RabbitMqServiceImpl implements RabbitMqService, RabbitTemplate.ConfirmCallback,RabbitTemplate.ReturnCallback {

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 当然，init方法在spring自动注入rabbitTemplate之后执行
     */
    @PostConstruct
    public void init(){
        rabbitTemplate.setConfirmCallback(this);
        rabbitTemplate.setReturnCallback(this);
    }

    /**
     * String exchange 交换机名称
     * String routingKey 路由Key
     * Object object 具体发送的消息
     * @param message
     */
    @Override
    public void sendMessage(String message) {
        //发送的时候携带的数据，唯一标识
        CorrelationData correlationData=new CorrelationData("3453");

        //amqpTemplate.convertAndSend(RabbitMQConfig.DIRECT_EXCHANGE,RabbitMQConfig.DIRECT_ROUTINGKEY,message);
        //rabbitTemplate.convertAndSend(RabbitMQConfig.DIRECT_EXCHANGE,RabbitMQConfig.DIRECT_ROUTINGKEY,message, correlationData);
        // 这里弄个错误的routing key进行测试，路由不到，这种消息不可达
        rabbitTemplate.convertAndSend(RabbitMQConfig.DIRECT_EXCHANGE,RabbitMQConfig.DIRECT_ROUTINGKEY,message, correlationData);
    }
    /**
     *发送广播消息
     */
    @Override
    public void sendFanoutMessage(String message) {
        amqpTemplate.convertAndSend(RabbitMQConfig.FANOUT_EXCHANGE,"",message);
    }

    @Override
    public void sendRoutingMessage() {
        amqpTemplate.convertAndSend(RabbitMQConfig.DIRECT_EXCHANGE1,"error","发送error级别的消息");
    }

    /**
     * exchange: TOPIC_EXCHANGE
     * routingkey模糊匹配: quick.orange.rabbit
     * message: 飞快的橘色兔子
     */
    @Override
    public void sendTopicMessage() {
        amqpTemplate.convertAndSend(RabbitMQConfig.TOPIC_EXCHANGE,"quick.orange.rabbit","飞快的橘色兔子");
        //amqpTemplate.convertAndSend(RabbitMQConfig.TOPIC_EXCHANGE,"lazy.orange.elephant",",慢吞吞的橘色大象");
        // amqpTemplate.convertAndSend(RabbitMQConfig.TOPIC_EXCHANGE,"quick.orange.fox","quick.orange.fox");
        // amqpTemplate.convertAndSend(RabbitMQConfig.TOPIC_EXCHANGE,"lazy.brown.fox","lazy.brown.fox");
        //amqpTemplate.convertAndSend(RabbitMQConfig.TOPIC_EXCHANGE,"quick.brown.fox","quick.brown.fox");

    }

    /**
     *
     * @param correlationData  消息唯一标识, 存在的意义便是：如果消息发送失败，可以根据这个标识补发消息
     * @param ack 交换机是否成功收到消息 true成功  false失败
     * @param cause 失败原因
     */
    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {

        System.out.println("confirm方法被执行了..."+correlationData);
        if(ack){
            System.out.println("交换机 ，消息接收成功"+cause);
        }else{
            System.out.println("交换机 ，消息接收失败"+cause);
            // 我们这里要做一些消息补发的措施
            System.out.println("id="+correlationData.getId());
        }

    }

    /**
     *
     * @param message 消息主体
     * @param replyCode 返回code
     * @param replyText 返回信息
     * @param exchange 交换机
     * @param routingKey 路由key
     */
    @Override
    public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {
        System.out.println("return方法被执行...");
        System.out.println("消息主体："+new String(message.getBody())); // 得到消息对象，通过id再进行消息的补发
        System.out.println("replyCode："+replyCode);
        System.out.println("replyText："+replyText);
        System.out.println("exchange："+exchange);
        System.out.println("routingKey："+routingKey);
    }

    /*@Override
    public void sendTTLMessage(String message) {
        MessageProperties messageProperties=new MessageProperties();
        messageProperties.setExpiration("15000"); // 设置过期时间  15秒
        Message msg=new Message(message.getBytes(),messageProperties);
        rabbitTemplate.send(RabbitMQConfig.TTL_DIRECT_EXCHANGE,RabbitMQConfig.TTL_DIRECT_ROUTINGKEY,msg);
    }*/
    @Override
    public void sendTTLMessage(String message) {
        rabbitTemplate.convertAndSend(RabbitMQConfig.TTL_DIRECT_EXCHANGE,RabbitMQConfig.TTL_DIRECT_ROUTINGKEY,message);
    }

    @Override
    public void sendDelayedMessage(String message, Integer delayTime) {
        amqpTemplate.convertAndSend(RabbitMQConfig.DELAYED_DIRECT_EXCHANGE,RabbitMQConfig.DELAYED_DIRECT_ROUTINGKEY,message,a->{
            a.getMessageProperties().setDelay(delayTime);
            return a;
        });
    }
}
