package com.java1234.producer.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;


@Configuration
public class RabbitMQConfig {

    /**
     * direct交换机名称
     */
    public static final String DIRECT_EXCHANGE="directExchange";

    /**
     * fanout交换机名称  ( fanout: 广播)
     */
    public static final String FANOUT_EXCHANGE="fanoutExchange";

    /**
     * direct交换机名称1
     */
    public static final String DIRECT_EXCHANGE1="directExchange1";

    //********************************************

    /**
     * direct队列名称
     */
    public static final String DIRECT_QUEUE="directQueue";

    /**
     * direct路由Key
     */
    public static final String DIRECT_ROUTINGKEY="directRoutingKey";

    /**
     * 订阅队列1名称
     */
    public static final String SUB_QUEUE1="subQueue1";

    /**
     * 订阅队列2名称
     */
    public static final String SUB_QUEUE2="subQueue2";


    /**
     * direct队列名称1
     */
    public static final String DIRECT_QUEUE1="directQueue1";

    /**
     * direct队列名称2
     */
    public static final String DIRECT_QUEUE2="directQueue2";

    //**********************************************************

    /**
     * 定义一个direct交换机
     * @return
     */
    @Bean
    public DirectExchange directExchange(){
        return new DirectExchange(DIRECT_EXCHANGE);
    }

    /**
     * 定义一个fanout交换机
     * @return
     */
    @Bean
    public FanoutExchange fanoutExchange(){
        return new FanoutExchange(FANOUT_EXCHANGE);
    }

    /**
     * 定义一个direct交换机1
     * @return
     */
    @Bean
    public DirectExchange directExchange1(){
        return new DirectExchange(DIRECT_EXCHANGE1);
    }

    //***********************************************************************

    /**
     * 定义一个direct队列
     * @return
     */
    @Bean
    public Queue directQueue(){
        return new Queue(DIRECT_QUEUE);
    }

    /**
     * 定义一个订阅队列1
     * @return
     */
    @Bean
    public Queue subQueue1(){
        return new Queue(SUB_QUEUE1);
    }

    /**
     * 定义一个订阅队列2
     * @return
     */
    @Bean
    public Queue subQueue2(){
        return new Queue(SUB_QUEUE2);
    }

    /**
     * 定义一个direct1队列
     * @return
     */
    @Bean
    public Queue directQueue1(){
        return new Queue(DIRECT_QUEUE1);
    }

    /**
     * 定义一个direct2队列
     * @return
     */
    @Bean
    public Queue directQueue2(){
        return new Queue(DIRECT_QUEUE2);
    }

    //*************************************************

    /**
     * 定义一个队列和交换机的绑定
     * @return
     */
    @Bean
    public Binding directBinding(){
        return BindingBuilder.bind(directQueue()).to(directExchange()).with(DIRECT_ROUTINGKEY);
    }

    /**
     * 定义一个队列和交换机的绑定
     *  广播： 由于交换机会把消息发送到所有绑定到这个交换机上的队列中，所有这种模式无需 ROUTINGKEY
     * @return
     */
    @Bean
    public Binding fanoutBinding1(){
        return BindingBuilder.bind(subQueue1()).to(fanoutExchange());
    }

    /**
     * 定义一个队列和交换机的绑定
     *  广播： 由于交换机会把消息发送到所有绑定到这个交换机上的队列中，所有这种模式无需 ROUTINGKEY
     * @return
     */
    @Bean
    public Binding fanoutBinding2(){
        return BindingBuilder.bind(subQueue2()).to(fanoutExchange());
    }



    /**
     * 定义一个队列和交换机的绑定
     * @return
     */
    @Bean
    public Binding directBinding1(){
        return BindingBuilder.bind(directQueue1()).to(directExchange1()).with("error");
    }

    /**
     * 定义一个队列和交换机的绑定
     * @return
     */
    @Bean
    public Binding directBinding2(){
        return BindingBuilder.bind(directQueue2()).to(directExchange1()).with("info");
    }

    /**
     * 定义一个队列和交换机的绑定
     * @return
     */
    @Bean
    public Binding directBinding3(){
        return BindingBuilder.bind(directQueue2()).to(directExchange1()).with("error");
    }

    /**
     * 定义一个队列和交换机的绑定
     * @return
     */
    @Bean
    public Binding directBinding4(){
        return BindingBuilder.bind(directQueue2()).to(directExchange1()).with("warning");
    }


    /**
     * topic队列名称1
     */
    public static final String TOPIC_QUEUE1="topicQueue1";

    /**
     * topic队列名称2
     */
    public static final String TOPIC_QUEUE2="topicQueue2";

    /**
     * TOPIC交换机名称
     */
    public static final String TOPIC_EXCHANGE="topicExchange";

    /**
     * 定义一个topic队列1
     * @return
     */
    @Bean
    public Queue topicQueue1(){
        return new Queue(TOPIC_QUEUE1);
    }

    /**
     * 定义一个topic队列2
     * @return
     */
    @Bean
    public Queue topicQueue2(){
        return new Queue(TOPIC_QUEUE2);
    }

    /**
     * 定义一个topic交换机
     * @return
     */
    @Bean
    public TopicExchange topicExchange(){
        return new TopicExchange(TOPIC_EXCHANGE);
    }

    /**
     * 定义一个队列和交换机的绑定
     * @return
     */
    @Bean
    public Binding topicBinding1(){
        // * 匹配一个单词
        return BindingBuilder.bind(topicQueue1()).to(topicExchange()).with("*.orange.*");
    }

    /**
     * 定义一个队列和交换机的绑定
     * @return
     */
    @Bean
    public Binding topicBinding2(){
        // * 匹配一个单词
        return BindingBuilder.bind(topicQueue2()).to(topicExchange()).with("*.*.rabbit");
    }

    /**
     * 定义一个队列和交换机的绑定
     * @return
     */
    @Bean
    public Binding topicBinding3(){
        // # 匹配零个或者多个单词
        return BindingBuilder.bind(topicQueue2()).to(topicExchange()).with("lazy.#");
    }

    //******************** TTL  *************************
    /**
     * TTL_direct交换机名称
     */
    public static final String TTL_DIRECT_EXCHANGE="ttldirectExchange";

    /**
     * ttl_direct路由Key
     */
    public static final String TTL_DIRECT_ROUTINGKEY="ttl_directRoutingKey";

    /**
     * ttl_direct队列名称
     */
    public static final String TTL_DIRECT_QUEUE="ttldirectQueue";

    /**
     * 定义一个TTL direct交换机
     * @return
     */
    @Bean
    public DirectExchange ttlDirectExchange(){
        return new DirectExchange(TTL_DIRECT_EXCHANGE);
    }

    /**
     * 定义一个TTL direct队列
     * @return
     */
    /*@Bean
    public Queue ttlDirectQueue(){

        return new Queue(TTL_DIRECT_QUEUE);
    }*/
    /*@Bean
    public Queue ttlDirectQueue(){
        Map<String,Object> map=new HashMap<>();
        map.put("x-message-ttl",5000);
        return new Queue(TTL_DIRECT_QUEUE,true,false,false,map);
    }*/
    @Bean
    public Queue ttlDirectQueue(){
        Map<String,Object> map=new HashMap<>();
        map.put("x-message-ttl",1000000);
        map.put("x-dead-letter-exchange",DLX_DIRECT_EXCHANGE);
        map.put("x-dead-letter-routing-key",DLX_DIRECT_ROUTINGKEY);
        map.put("x-max-length",100);   // 队列中最多消息数量限制
        return new Queue(TTL_DIRECT_QUEUE,true,false,false,map);
    }

    /**
     * TTL定义一个队列和交换机的绑定
     * @return
     */
    @Bean
    public Binding ttlDirectBinding(){
        return BindingBuilder.bind(ttlDirectQueue()).to(ttlDirectExchange()).with(TTL_DIRECT_ROUTINGKEY);
    }

    // ***************** DLX ***************************

    /**
     * DLX_direct交换机名称
     */
    public static final String DLX_DIRECT_EXCHANGE="dlxldirectExchange";

    /**
     * dlx_direct队列名称
     */
    public static final String DLX_DIRECT_QUEUE="dlxldirectQueue";

    /**
     * dlx_direct路由Key
     */
    public static final String DLX_DIRECT_ROUTINGKEY="dlx_directRoutingKey";

    /**
     * 定义一个DLX direct交换机
     * @return
     */
    @Bean
    public DirectExchange dlxDirectExchange(){
        return new DirectExchange(DLX_DIRECT_EXCHANGE);
    }

    /**
     * 定义一个DLX direct队列
     * @return
     */
    @Bean
    public Queue dlxDirectQueue(){
        return new Queue(DLX_DIRECT_QUEUE);
    }

    /**
     * dlx定义一个队列和交换机的绑定
     * @return
     */
    @Bean
    public Binding dlxDirectBinding(){
        return BindingBuilder.bind(dlxDirectQueue()).to(dlxDirectExchange()).with(DLX_DIRECT_ROUTINGKEY);
    }

    // ************  延迟消息 **********************
    /**
     * delayedDirect交换机名称
     */
    public static final String DELAYED_DIRECT_EXCHANGE="delayedDirectExchange";

    /**
     * delayed direct队列名称
     */
    public static final String DELAYED_DIRECT_QUEUE="delayedDirectQueue";

    /**
     * delayed_direct路由Key
     */
    public static final String DELAYED_DIRECT_ROUTINGKEY="delayed_directRoutingKey";

    /**
     * 定义一个delayed direct交换机
     * @return
     */
    @Bean
    public CustomExchange delayedDirectExchange(){
        Map<String, Object> args = new HashMap<>();
        args.put("x-delayed-type", "direct");
        return new CustomExchange(DELAYED_DIRECT_EXCHANGE,"x-delayed-message", true, false, args);

    }

    /**
     * 定义一个DELAYED direct队列
     * @return
     */
    @Bean
    public Queue delayedDirectQueue(){
        return new Queue(DELAYED_DIRECT_QUEUE);
    }

    /**
     * 定义一个队列和交换机的绑定
     * @return
     */
    @Bean
    public Binding delayedDirectBinding(){
        return BindingBuilder.bind(delayedDirectQueue()).to(delayedDirectExchange()).with(DELAYED_DIRECT_ROUTINGKEY).noargs();
    }
}
